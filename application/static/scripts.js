
// for: application/templates/snippets/usermenu
$('#logout-link').click(function (e) {
    e.preventDefault();
    $('#logout-form').submit();
});

// chat box on every page
((window.gitter = {}).chat = {}).options = {
    room: 'painless-software'
};
