[tox]
envlist =
    flake8
    pylint
    bandit
    safety
    kubernetes
    py37
    behave
    requirements
skipsdist = true

[testenv]
description = Unit tests
deps =
    pytest-cov
    -r {toxinidir}/requirements.txt
commands =
    pytest {posargs:--cov=application}
setenv =
    DJANGO_SECRET_KEY = testing

[testenv:bandit]
description = PyCQA security linter
deps = bandit<1.6.0
commands = bandit -r --ini tox.ini

[testenv:behave]
description = Acceptence tests (BDD)
deps =
    behave-django
    -r {toxinidir}/requirements.txt
commands =
    python manage.py behave {posargs}
setenv =
    DJANGO_DATABASE_URL = sqlite://
    DJANGO_SECRET_KEY = testing

[testenv:clean]
description = Remove bytecode and other debris
deps = pyclean
commands =
    py3clean -v {toxinidir}
    rm -rf .cache/ .pytest_cache/ .tox/ tests/reports/
whitelist_externals =
    rm

[testenv:flake8]
description = Static code analysis and code style
deps = flake8-django
commands = flake8 {posargs}

[testenv:kubernetes]
description = Validate Kubernetes manifests
deps = kustomize-wrapper
commands =
    # generate directory list:
    # $ ls -d1 deployment/*/overlays/* | sed 's/$/ \\/'
    kustomize lint {posargs:--ignore-missing-schemas --fail-fast \
        deployment/application/overlays/development \
        deployment/application/overlays/integration \
        deployment/application/overlays/production \
        deployment/database/overlays/development \
        deployment/database/overlays/integration \
        deployment/database/overlays/production \
    }

[testenv:pylint]
description = Check for errors and code smells
deps =
    pylint-django
    -r {toxinidir}/requirements.txt
commands =
    # generate module list:
    # $ ls */__init__.py | sed 's#/__init__.py# \\#g'
    pylint --rcfile tox.ini {posargs:manage.py \
        application \
        content \
    }

[testenv:requirements]
description = Update project dependencies
deps = pip-tools
commands = pip-compile --output-file=requirements.txt requirements.in --upgrade

[testenv:safety]
description = Check for vulnerable dependencies
deps = safety
commands = safety check --bare -r requirements.txt

[bandit]
exclude = .cache,.git,.tox,build,dist,docs,tests
targets = .

[behave]
# default_format = progress
default_tags = -@not_implemented -@xfail
junit = yes
junit_directory = tests/reports
paths = tests/acceptance
show_skipped = no
summary = no

[flake8]
exclude = .cache,.git,.tox,build

[pylint]
[MASTER]
load-plugins = pylint_django
output-format = colorized

[pytest]
addopts =
    --color=yes
    --doctest-modules
    --ignore=application/urls.py
    --ignore=application/wsgi.py
    --ignore=tests/acceptance/steps
    --junitxml=tests/reports/unittests.xml
    --strict
    --verbose
