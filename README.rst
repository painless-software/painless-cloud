Painless Cloud
==============

Turn-key continuous delivery at your fingertips.

Getting Started
---------------

To start developing on this project simply bring up the Docker setup:

.. code-block:: console

    docker-compose build
    docker-compose up

Migrations will run automatically at startup (via the container entrypoint).
If they fail the very first time simply restart the application.

Open your web browser at http://localhost:8000 to see the application
you're developing.  Log output will be displayed in the terminal, as usual.

For running tests, linting, security checks, etc. see instructions in the
`tests/ <tests/README.rst>`_ folder.

Initial Setup
^^^^^^^^^^^^^

#. Create a project at the
   `VSHN Control Panel <https://control.vshn.net/openshift/projects/appuio%20public>`_.
   For quota sizing consider roughly the sum of ``limits`` of all
   resources (must be strictly greater than the sum of ``requests``):

   .. code-block:: console

        grep -A2 limits deployment/*/*/*yaml
        grep -A2 requests deployment/*/*/*yaml

#. With the commands below, create a service account from your terminal
   (logging in to your cluster first), grant permissions to push images
   and apply configurations, and get the service account's token value:
   (`APPUiO docs <https://docs.appuio.ch/en/latest/services/webserver/50_pushing_to_appuio.html>`_)

   .. code-block:: console

        oc -n painless-cloud create sa gitlab-ci
        oc -n painless-cloud policy add-role-to-user admin -z gitlab-ci
        oc -n painless-cloud sa get-token gitlab-ci

#. Use the service account token to configure the
   `Kubernetes integration <https://gitlab.com/painless-software/painless-cloud/-/clusters>`_
   of your GitLab project: (`GitLab docs <https://docs.gitlab.com/ee/user/project/clusters/>`_)

   - Operations > Kubernetes > "APPUiO" > Kubernetes cluster details > Service Token

   and ensure the following values are set in the cluster details:

   - RBAC-enabled cluster: *(checked)*
   - GitLab-managed cluster: *(unchecked)*
   - Project namespace: "painless-cloud"

Integrate External Tools
^^^^^^^^^^^^^^^^^^^^^^^^

:Sentry:
  - Add environment variable ``SENTRY_DSN`` in
    `Settings > CI/CD > Variables <https://gitlab.com/painless-software/painless-cloud/-/settings/ci_cd>`_
  - Delete secrets in your namespace and run a deployment (to recreate them)
  - Configure `Error Tracking <https://gitlab.com/painless-software/painless-cloud/-/error_tracking>`_
    in `Settings > Operations > Error Tracking <https://gitlab.com/painless-software/painless-cloud/-/settings/operations>`_

Working with Docker
^^^^^^^^^^^^^^^^^^^

Create/destroy development environment:

.. code-block:: console

    docker-compose up -d    # create and start; omit -d to see log output
    docker-compose down     # docker-compose kill && docker-compose rm -af

Start/stop development environment:

.. code-block:: console

    docker-compose start    # resume after 'stop'
    docker-compose stop     # stop containers, but keep them intact

Other useful commands:

.. code-block:: console

    docker-compose ps       # list running containers
    docker-compose logs -f  # view (and follow) container logs

See the `docker-compose CLI reference`_ for other commands.

.. _docker-compose CLI reference: https://docs.docker.com/compose/reference/overview/

CI/CD Process
^^^^^^^^^^^^^

We have 3 environments corresponding to 3 deployments in a single namespace
on our container platform: *development*, *integration*, *production*

- Any merge request triggers a deployment of a review app on *development*.
  When a merge request is merged or closed the review app will automatically
  be removed.
- Any change on the main branch, e.g. when a merge request is merged into
  ``master``, triggers a deployment on *integration*.
- To trigger a deployment on *production* push a Git tag, e.g.

  .. code-block:: console

    git checkout master
    git tag 1.0.0
    git push --tags

Credits
^^^^^^^

Made with ♥ by `Painless Continuous Delivery`_ Cookiecutter. This project was
generated via:

.. code-block:: console

    cookiecutter gh:painless-software/painless-continuous-delivery \
        project_name="Painless Cloud" \
        project_description="Turn-key continuous delivery at your fingertips." \
        vcs_platform="GitLab.com" \
        vcs_account="painless-software" \
        vcs_project="painless-cloud" \
        ci_service=".gitlab-ci.yml" \
        cloud_platform="APPUiO" \
        cloud_account="demo4501@appuio.ch" \
        cloud_project="painless-cloud" \
        environment_strategy="shared" \
        docker_registry="registry.appuio.ch" \
        framework="Django" \
        database="Postgres" \
        cronjobs="(none)" \
        checks="flake8,pylint,bandit,safety,kubernetes" \
        tests="py37,behave" \
        monitoring="Sentry" \
        license="Apache-2" \
        --no-input

.. _Painless Continuous Delivery: https://github.com/painless-software/painless-continuous-delivery/
